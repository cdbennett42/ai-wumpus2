package src;

class tiles {
	static int GRID_SIZE = 10;
	int safe = 0;
	int unsafe = 0;
	int wump = 0;
	int sup = 0;
	int pit = 0;
	int gold = 0;
	int doubt_pit = 0;
	int doubt_wump = 0;
	int doubt_sup = 0;
	int doubt_gold = 0;
	String env;
	int num = 0;
	int br = 0;
	int bl = 0;
	int bu = 0;
	int bd = 0;
	int visited = 0;
	int revisited = 0;
	int l, r, u, d;
	String back = "";

	tiles(String s, int n) {
		env = s;
		num = n;
		l = r = u = d = 0;
		
		for (int i = 1; i < GRID_SIZE-1; i++)	//can the player move left?
			if (n == ((GRID_SIZE*i)+1))
				bl = 1;
		
		for (int i = 2; i < GRID_SIZE; i++)	//can the player move right?
			if (n == GRID_SIZE*i)
				br = 1;
		
		//is the player in one of the corners?
		if (n == 1) {
			bu = 1;
			bl = 1;
		}
		else if (n == (GRID_SIZE*GRID_SIZE)) {
			br = 1;
			bd = 1;
		}
		else if (n == GRID_SIZE){
			br = 1;
			bu = 1;
		}
		else if (n == ((GRID_SIZE*(GRID_SIZE-1)) +1)){
			bl = 1;
			bd = 1;
		}

	}
	
	boolean beenVisited()
	{
		return (visited > 0);
	}
	boolean traveledBackOn()
	{
		return (revisited > 1);
	}

	int sense() {
		if (env.contains("B"))
			return 1;
		else if (env.contains("H"))
			return 2;
		else if (env.contains("G"))
			return 3;
		else if (env.contains("W"))
			return 4;
		else if (env.contains("M"))
			return 5;
		else if (env.contains("S"))
			return 6;
		else if (env.contains("T"))
			return 7;
		
		else
			return 0;
	}

}