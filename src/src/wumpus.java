//import java.util.Scanner;
package src;

import java.util.Scanner;

class wumpus {
	static int scream = 0;
	static int score = 0;
	static int complete = 0;
	static int GRID_SIZE = 10;
	static boolean supmuw_bad = false;
	static boolean has_shared = false;

	static boolean check(tiles t) {
		int temp = t.sense();
		if (temp == 1 || temp == 2)
			return false;

		return true;
	}
	
	private static tiles[] lookAround(tiles[] t, int pos)
	{
		int condition = t[pos].sense();
		if (condition == 1 && t[pos].visited == 0) {
			if (t[pos].br != 1 && t[pos + 1].safe != 1)
				t[pos + 1].doubt_pit += 1;
			if (t[pos].bu != 1 && (pos - GRID_SIZE) >= 1 && t[pos - GRID_SIZE].safe != 1)
				t[pos - GRID_SIZE].doubt_pit += 1;
			if (t[pos].bl != 1 && t[pos - 1].safe != 1)
				t[pos - 1].doubt_pit += 1;
			if (t[pos].bd != 1 && (pos + GRID_SIZE) <= (GRID_SIZE*GRID_SIZE) && t[pos + GRID_SIZE].safe != 1)
				t[pos + GRID_SIZE].doubt_pit += 1;

			t[pos].safe = 1;
		} else if (condition == 2 && t[pos].visited == 0) {
			if (t[pos].br != 1 && t[pos + 1].safe != 1)
				t[pos + 1].doubt_wump += 1;
			if (t[pos].bu != 1 && (pos - GRID_SIZE) >= 1 && t[pos - GRID_SIZE].safe != 1)
				t[pos - GRID_SIZE].doubt_wump += 1;
			if (t[pos].bl != 1 && t[pos - 1].safe != 1)
				t[pos - 1].doubt_wump += 1;
			if (t[pos].bd != 1 && (pos + GRID_SIZE) <= (GRID_SIZE*GRID_SIZE) && t[pos + GRID_SIZE].safe != 1)
				t[pos + GRID_SIZE].doubt_wump += 1;

			t[pos].safe = 1;
		} else if (condition == 5 && t[pos].visited == 0) {
			if (t[pos].br != 1 && t[pos + 1].safe != 1)
				t[pos + 1].doubt_sup += 1;
			if (t[pos].bu != 1 && (pos - GRID_SIZE) >= 1 && t[pos - GRID_SIZE].safe != 1)
				t[pos - GRID_SIZE].doubt_sup += 1;
			if (t[pos].bl != 1 && t[pos - 1].safe != 1)
				t[pos - 1].doubt_sup += 1;
			if (t[pos].bd != 1 && (pos + GRID_SIZE) <= (GRID_SIZE*GRID_SIZE) && t[pos + GRID_SIZE].safe != 1)
				t[pos + GRID_SIZE].doubt_sup += 1;
			
			t[pos].safe = 1;
		} else if (condition == 7 && t[pos].visited == 0) {
			if (t[pos].br != 1 && t[pos + 1].safe != 1)
				t[pos + 1].doubt_gold += 1;
			if (t[pos].bu != 1 && (pos - GRID_SIZE) >= 1 && t[pos - GRID_SIZE].safe != 1)
				t[pos - GRID_SIZE].doubt_gold += 1;
			if (t[pos].bl != 1 && t[pos - 1].safe != 1)
				t[pos - 1].doubt_gold += 1;
			if (t[pos].bd != 1 && (pos + GRID_SIZE) <= (GRID_SIZE*GRID_SIZE) && t[pos + GRID_SIZE].safe != 1)
				t[pos + GRID_SIZE].doubt_gold += 1;
			
			t[pos].safe = 1;
		}
		else if (condition == 0)
			t[pos].safe = 1;
		
		t[pos].visited = 1;
		return t;
	}
	
	private static boolean safeToGo(char direction, tiles[] t, int pos)
	{
		int add_to = 0;
		if (direction == 'u') add_to -= GRID_SIZE;
		else if (direction == 'd') add_to += GRID_SIZE;
		else if (direction == 'l') add_to--;
		else if (direction == 'r') add_to++;
		
		if((pos + add_to) < 0 || (pos + add_to) > (GRID_SIZE*GRID_SIZE))
			return false;
		
		String s = "" + direction;
		return (
				((t[pos].br != 1 && t[pos].r != 1) || direction != 'r') && 
				((t[pos].bl != 1 && t[pos].l != 1) || direction != 'l') && 
				((t[pos].bu != 1 && t[pos].u != 1) || direction != 'u') && 
				((t[pos].bd != 1 && t[pos].d != 1) || direction != 'd') && 
				
				t[pos + add_to].doubt_pit < 1 && 
				t[pos + add_to].doubt_wump < 1 &&
				(t[pos + add_to].doubt_sup < 1 && !supmuw_bad) &&
				t[pos + add_to].pit != 1 && 
				t[pos + add_to].wump != 1 && 
				(t[pos + add_to].sup != 1 && !supmuw_bad) &&
				
				!(t[pos].back.contains(s) && 
						((t[pos].l != 1 || direction == 'l') || 
						(t[pos].u != 1 || direction == 'u') ||
						(t[pos].d != 1 || direction == 'd') ||
						(t[pos].r != 1 || direction == 'r')) && 
						check(t[pos]))
				);
	}

	public static void main(String args[]) {
		//Scanner scr = new Scanner(System.in);
		Environment e = new Environment();
		String w[][] = new String[GRID_SIZE+2][GRID_SIZE+2];
		e.accept(w);
		System.out.println("\n\nFinding the solution...");
		
		tiles t[] = new tiles[(GRID_SIZE*GRID_SIZE)+1];
		int c = 1;
		out: for (int i = 1; i <= GRID_SIZE; ++i) {
			for (int j = 1; j <= GRID_SIZE; ++j) {
				if (c > (GRID_SIZE*GRID_SIZE))
					break out;
				t[c] = new tiles(w[i][j], c);
				++c;
			}
		}

		t[(GRID_SIZE*(GRID_SIZE-1))+1].safe = 1;
		t[(GRID_SIZE*(GRID_SIZE-1))+1].visited = 1;
		
		for(int i=0; i< GRID_SIZE; i++)
		{
			for(int j=0; j< GRID_SIZE; j++)
			{
				int position = ((j*10) + i + 1);
				if (t[position].env.contains("W") || (t[position].env.contains("H")))
					if (t[position].env.contains("S"))
					{
						supmuw_bad = true;
						break;
					}
			}
			if (supmuw_bad)
				break;
		}


		int pos = (GRID_SIZE*(GRID_SIZE-1)+1);
		int condition;
		int limit = 0;
		String temp1;
		do {
			++limit;
			condition = -1;

			if (t[pos].env.contains("G")) {
				complete = 1;
				System.out.println("Gold Found!!");
				break;
			}

			if (safeToGo('r', t, pos)) {
				
				temp1 = "l";
				t[pos].r = 1; 
				++pos;
				System.out.println("right pos=" + pos);
				--score;
				t[pos].back += temp1;
				
				condition = t[pos].sense();
				if (condition == 3) {
					complete = 1;
					break;
				} else {
					t = lookAround(t, pos);
				}

				t[pos].visited = 1;
			} else if (safeToGo('l', t, pos)) {
				temp1 = "r";
				t[pos].l = 1;
				pos = pos - 1;
				System.out.println("left pos= " + pos);
				--score;
				t[pos].back += temp1;


				condition = t[pos].sense();
				if (condition == 3) {
					complete = 1;
					break;
				} else {
					t = lookAround(t, pos);
				}

			} else if (safeToGo('u', t, pos)) {
				
				temp1 = "d";
				t[pos].u = 1;
				pos = pos - GRID_SIZE;
				System.out.println("Up pos= " + pos);
				--score;
				t[pos].back += temp1;

				condition = t[pos].sense();
				if (condition == 3) {
					complete = 1;
					break;
				} else {
					t = lookAround(t, pos);
				}
				
				t[pos].visited = 1;
			} else if (safeToGo('d', t, pos)) {
				temp1 = "u";
				t[pos].d = 1;
				pos = pos + GRID_SIZE;
				System.out.println("down pos= " + pos);
				--score;
				t[pos].back += temp1;
				
				condition = t[pos].sense();
				if (condition == 3) {
					complete = 1;
					break;
				} else {
					t = lookAround(t, pos);
				}

				t[pos].visited = 1;
			} else if (limit > 50) {
				int temp3 = pos;
				int flag_1 = 0, flag2 = 0, flag3 = 0, flag4 = 0;

				while (t[pos].visited == 1 && t[pos].br != 1) {
					++pos;
					--score;
					System.out.println("right pos= " + pos);
				}

				if (t[pos].pit == 1 || t[pos].wump == 1
						|| (t[pos].br == 1 && t[pos].visited == 1 && t[pos].safe != 1)) {
					pos = temp3;
					flag_1 = 1;
				}

				if (flag_1 == 0)
					t[pos].back += "l";

				while (pos + GRID_SIZE >= 1 && t[pos].bu != 1 && t[pos].visited == 1) {
					pos -= GRID_SIZE;
					--score;
					System.out.println("up pos= " + pos);
				}

				if (t[pos].pit == 1 || t[pos].wump == 1
						|| (t[pos].bu == 1 && t[pos].visited == 1 && t[pos].safe != 1)) {
					pos = temp3;
					flag3 = 1;
				}

				if (flag3 == 0)
					t[pos].back += "d";

				while (t[pos].visited == 1 && t[pos].bl != 1) {
					--pos;
					--score;
					System.out.println("left pos= " + pos);
				}

				if (t[pos].pit == 1 || t[pos].wump == 1
						|| (t[pos].bl == 1 && t[pos].visited == 1 && t[pos].safe != 1)) {
					pos = temp3;
					flag2 = 1;
				}

				if (flag2 == 0)
					t[pos].back += "r";

				while (pos + GRID_SIZE <= (GRID_SIZE*GRID_SIZE) && t[pos].bd != 1 && t[pos].visited == 1) {
					pos += GRID_SIZE;
					--score;
					System.out.println("down pos= " + pos);
				}

				if (t[pos].pit == 1 || t[pos].wump == 1
						|| (t[pos].bd == 1 && t[pos].visited == 1 && t[pos].safe != 1)) {
					pos = temp3;
					flag4 = 1;
				}

				if (flag4 == 0)
					t[pos].back += "u";

				t[pos].safe = 1;
				t[pos].visited = 1;
				limit = 0;
			}
			if (t[pos].env.contains("W") && scream != 1) {
				score -= 10;
				scream = 1;
				t[pos].safe = 1;
				System.out.println("\n\nWumpus killed >--0-->");
				t[pos].env.replace("W", " ");
				for (int l = 1; l <= (GRID_SIZE*GRID_SIZE); ++l) {
					t[l].doubt_wump = 0;
					t[l].env.replace("H", " ");
				}
			}
			
			if (t[pos].env.contains("S") && !supmuw_bad && !has_shared) {
				has_shared = true;
				score += 50;
				t[pos].sup = 1;
				System.out.println("\n\nYou have shared food with a supmuw " + pos + ".");
			}
			
			if (t[pos].env.contains("S") && supmuw_bad) {
				has_shared = true;
				score -= 1000;
				t[pos].sup = 1;
				System.out.println("\n\nYou have been killed by a supmuw " + pos + ".");
				complete = 2;
				break;
			}

			if (t[pos].env.contains("P")) {
				if (t[pos].env.contains("S")) {
					System.out.println("\n\nSupmuw has kept you out of the pit at position " + pos + ".");
					t[pos].safe = 1;
				} else {
				score -= 1000;
				t[pos].pit = 1;
				System.out.println("\n\nFallen in pit of position " + pos + ".");
				complete = 2;
				break;
				}
			}

			for (int k = 1; k <= (GRID_SIZE*GRID_SIZE); ++k) {
				if (t[k].doubt_pit == 1 && t[k].doubt_wump == 1) {
					t[k].doubt_pit = 0;
					t[k].doubt_wump = 0;
					t[k].safe = 1;
				}
			}

			for (int y = 1; y <= (GRID_SIZE*GRID_SIZE); ++y) {
				if (t[y].doubt_wump > 1) {
					t[y].wump = 1;
					for (int h = 1; h <= (GRID_SIZE*GRID_SIZE); ++h) {
						if (h != y) {
							t[h].doubt_wump = 0;
							t[h].env.replace("H", " ");
						}
					}

				}

			}

			for (int y = 1; y <= (GRID_SIZE*GRID_SIZE); ++y) {
				if (t[y].doubt_pit > 1) {
					t[y].pit = 1;
				}
			}

			try {
				Thread.sleep(200);
			} catch (Exception p) {
			}

		} while (complete == 0);
		
		if (complete == 2){
			System.out.println("The Agent has lost!");
			System.out.println("Final Score: " + score);
		}
		
		if (complete == 1) {
			System.out.println("The Gold has been found! The agent will now return to the exit.");
			
			boolean justmovedright = false;
			boolean justmovedup = false;
			
			do
			{
				score--;
				
				if (pos+10 < (GRID_SIZE*GRID_SIZE) && t[pos + GRID_SIZE].safe > 0 && !justmovedup) //default move down
				{
					justmovedright = false;
					justmovedup = false;
					pos+=GRID_SIZE;
					System.out.println("down pos= " + pos);
				}
				else if (t[pos].bl == 0 && t[pos-1].safe > 0 && !justmovedright) //default2 move left
				{
					justmovedright = false;
					justmovedup = false;
					pos--;
					System.out.println("left pos= " + pos);
				}
				else if (t[pos].bd > 0)	//if on the bottom row, try and move left
				{
					if (t[pos-1].safe > 0 && !justmovedright)
					{
						justmovedright = false;
						justmovedup = false;
						pos--;
						System.out.println("left pos= " + pos);
					}
					else if (t[pos - GRID_SIZE].safe > 0)
					{
						justmovedup = true;
						pos-=GRID_SIZE;
						System.out.println("up pos= " + pos);
					}
					else 
					{
						justmovedright = true;
						pos++;
						System.out.println("right pos= " + pos);
					}
				}
				else if (t[pos].bl > 0) //if on the left row, try and move down
				{
					if (t[pos+GRID_SIZE].safe > 0 && !justmovedup)
					{
						justmovedright = false;
						justmovedup = false;
						pos+=GRID_SIZE;
						System.out.println("down pos= " + pos);
					}
					else if (t[pos++].safe > 0)
					{
						justmovedright = true;
						pos++;
						System.out.println("right pos= " + pos);
					}
					else 
					{
						justmovedup = true;
						pos-=GRID_SIZE;
						System.out.println("up pos= " + pos);
					}
				}
				else if (t[pos++].safe > 0 && t[pos].br == 0) //if nothing else looks good then move right
				{
					justmovedright = true;
					pos++;
					System.out.println("right pos= " + pos);
				}
				else //if nothing else looks good then move up
				{
					justmovedup = true;
					pos-=GRID_SIZE;
					System.out.println("up pos= " + pos);
				}
				t[pos].revisited = 1;
			} while (pos != (GRID_SIZE*(GRID_SIZE-1)+1));
			
			//pos = (GRID_SIZE*(GRID_SIZE-1)+1);
			score += 1000;
		}
		System.out.println("The final score of the agent reaching the exit with the gold is " + score);
		System.out.println("Here is what the final grid looks like");
		System.out.println("'-'s are Visited squares");
		//System.out.println("'='s are Revisited squares");
		System.out.println("'H's are howls from a Wumpus");
		System.out.println("'M's are moos from a Supmuw");
		System.out.println("'S's are Supmuws");
		System.out.println("'W's are Wumpuses");
		System.out.println("'G' is Gold");
		System.out.println("'T' is the Twinkle of Gold");
		System.out.println("'P's are pits");
		System.out.println("'B's are breezes from pits");
		System.out.println("'A' is the agent");
		
		finalDisplay(t, w);
		
		Scanner scr = new Scanner(System.in);
		String user = "N";
		do{
			System.out.println("\nEnter 'Y' to exit.");
			user = scr.next();
			user.toUpperCase();
		} while (user.contains("Y"));
		
		scr.close();
		
	}
	
	private static void printDividers()
	{
		System.out.printf("\n");
		for (int i=0; i<GRID_SIZE;i++)
			System.out.printf("----------------");
		System.out.printf("\n");
	}
	
	private static void finalDisplay(tiles[] t, String[][] w) {
		System.out.println("\nThe environment for problem is as follows.\n");
		for (int i = 1; i <= GRID_SIZE; ++i) {
			printDividers();
			System.out.print("|\t");
			for (int j = 1; j <= GRID_SIZE; ++j)
			{
				int position = ((i-1) * 10) + j;
				if (t[position].traveledBackOn())
					System.out.print(('=') + w[i][j] + "\t|\t");
				else if (t[position].beenVisited())
					System.out.print(('-') + w[i][j] + "\t|\t");
				else
					System.out.print(w[i][j] + "\t|\t");
			}
		}
		printDividers();
	}
	
}