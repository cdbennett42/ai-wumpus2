package src;

import java.util.Scanner;

class Environment {
	Scanner scr = new Scanner(System.in);
	// char w[][]=new char[5][5];
	int np; // number of pits
	int wp, gp, sp; // wumpus position gold position supmuw position
	int pos[]; // position of pits
	int b_pos[] = new int[20];
	int s_pos[] = new int[20];
	int m_pos[] = new int[20];
	int g_pos[] = new int[20];
	static int GRID_SIZE = 10;
	
	private void printDividers()
	{
		System.out.printf("\n");
		for (int i=0; i<GRID_SIZE;i++)
			System.out.printf("----------------");
		System.out.printf("\n");
	}

	void accept(String w[][]) {
		for (int i = 0; i < 20; ++i) {
			b_pos[i] = -1;
			s_pos[i] = -1;
			m_pos[i] = -1;
		}

		for (int i = 0; i <= GRID_SIZE; ++i)
			for (int j = 0; j <= GRID_SIZE; ++j)
				w[i][j] = "";

		int count = 1;
		System.out.println("\n\n********* Wumpus World Problem *********\n-by Aditya Mandhare.\n");

		System.out.println("The positions are as follows.");
		for (int i = 1; i <=GRID_SIZE; ++i) {
			printDividers();
			System.out.print("|\t");
			for (int j = 1; j <=GRID_SIZE; ++j)
				System.out.print((count++) + "\t|\t");
		}
		printDividers();
		System.out.println("\nAgent start position: " + ((GRID_SIZE*(GRID_SIZE-1)) +1));
		w[GRID_SIZE][1] = "A";
		System.out.println("\nEnter the number of pits.");
		np = scr.nextInt();
		pos = new int[np];
		System.out.println("Positions of pit, gold and wumpus should not overlap.");
		System.out.println("Enter the position of pits.");
		for (int i = 0; i < np; ++i) {
			pos[i] = scr.nextInt();
			show_sense(pos[i], 1, w);
		}
		System.out.println("Enter the position of wumpus.");
		wp = scr.nextInt();
		show_sense(wp, 2, w);
		
		System.out.println("Enter the position of supmuw.");
		sp = scr.nextInt();
		show_sense(sp, 3, w);

		System.out.println("Enter the position of gold.");
		gp = scr.nextInt();
		show_sense(gp, 4, w);

		insert(w);
	}

	void insert(String w[][]) {
		int temp = 0;
		int count = 0;
		int flag1 = 0, flag2 = 0, flag3 = 0;
		for (int i = 0; i < np; ++i) {
			temp = pos[i];
			count = 0;
			for (int j = 1; j <= GRID_SIZE; ++j) {
				for (int k = 1; k <= GRID_SIZE; ++k) {
					++count;
					if (count == temp)
						w[j][k] += "P";
					else if (count == gp && flag1 == 0) {
						w[j][k] += "G";
						flag1 = 1;
					} else if (count == wp && flag2 == 0) {
						w[j][k] += "W";
						flag2 = 1;
					}
					if (count == sp && flag3 == 0) {
						w[j][k] += "S";
						flag3 = 1;
					}
				}
			}
		}

		display(w);
	}

	void show_sense(int a, int b, String w[][]) {
		int t1, t2, t3, t4;
		t1 = a - 1;
		t2 = a + 1;
		t3 = a + GRID_SIZE;
		t4 = a - GRID_SIZE;
		
		for (int i = 1; i < GRID_SIZE-1; i++)	//can the player move left?
			if (a == ((GRID_SIZE*i)+1))
				t1 = 0;
		
		for (int i = 2; i < GRID_SIZE; i++)	//can the player move right?
			if (a == GRID_SIZE*i)
				t2 = 0;
		
		if (a == GRID_SIZE){
			t2 = 0;
		}
		if (a == ((GRID_SIZE*(GRID_SIZE-1)) +1)){
			t1 = 0;
		}
		
		
		if (t3 > (GRID_SIZE*GRID_SIZE)) { //can player move down?
			t3 = 0;
		}
		if (t4 < 0) { //can player move up?
			t4 = 0;
		}
		
		
		int s1, s2, s3, s4;
		s1 = a - 1 - GRID_SIZE; //lu
		s2 = a + 1 - GRID_SIZE; //ru
		s3 = a + 1 + GRID_SIZE; //rd
		s4 = a - 1 + GRID_SIZE; //ld
		
		if(t1 == 0 || t4 == 0)
			s1 = 0;
		if(t2 == 0 || t4 == 0)
			s2 = 0;
		if(t3 == 0 || t2 == 0)
			s3 = 0;
		if(t3 == 0 || t1 == 0)
			s4 = 0;

		
		
		// int temp[]=new int[4];

		if (b == 1) {
			b_pos[0] = t1;
			b_pos[1] = t2;
			b_pos[2] = t3;
			b_pos[3] = t4;
		} else if (b == 2) {
			m_pos[0] = t1;
			m_pos[1] = t2;
			m_pos[2] = t3;
			m_pos[3] = t4;
		} else if (b == 3) {
			s_pos[0] = t1;
			s_pos[1] = t2;
			s_pos[2] = t3;
			s_pos[3] = t4;
			s_pos[4] = s1;
			s_pos[5] = s2;
			s_pos[6] = s3;
			s_pos[7] = s4;
		} else if (b == 4) {
			g_pos[0] = t1;
			g_pos[1] = t2;
			g_pos[2] = t3;
			g_pos[3] = t4;
		}
		
		int temp1, count;

		for (int i = 0; i < 12; ++i) {
			if (b == 1)
				temp1 = b_pos[i];
			else if (b == 2)
				temp1 = m_pos[i];
			else if (b == 4)
				temp1 = g_pos[i];
			else 
				temp1 = s_pos[i];
			count = 0;
			for (int j = 1; j <= GRID_SIZE; ++j) {
				for (int k = 1; k <= GRID_SIZE; ++k) {
					++count;
					if (count == temp1 && b == 1 && !w[j][k].contains("B")) {
						w[j][k] += "B";
					} else if (count == temp1 && b == 2 && !w[j][k].contains("H"))
						w[j][k] += "H";
					else if (count == temp1 && b == 3 && !w[j][k].contains("M"))
						w[j][k] += "M";
					else if (count == temp1 && b == 4 && !w[j][k].contains("T"))
						w[j][k] += "T";
				}
			}
		}

		// display(w);
	}

	void display(String w[][]) {
		System.out.println("\nThe environment for problem is as follows.\n");
		for (int i = 1; i <= GRID_SIZE; ++i) {
			printDividers();
			System.out.print("|\t");
			for (int j = 1; j <= GRID_SIZE; ++j)
				System.out.print(w[i][j] + "\t|\t");
		}
		printDividers();
	}

}